#!/bin/bash

source ./libs/config.lib.sh
source ./libs/core.lib.sh

OUTPUT="\n"

for service in services.d/*; do
    res=$(${service})
    parse_plugin_return "${res}"

    OUTPUT=$OUTPUT$LINE
done

echo -e "${OUTPUT}" | column -t -s "|" --table-columns "NAME,TYPE,STATE,STARTUP" --table-empty-lines --table-order "NAME"
