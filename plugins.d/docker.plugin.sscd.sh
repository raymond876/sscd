#!/bin/bash

source ./libs/plugin.lib.sh

service_type="docker"

function get_service_status {
    local res
    res=$(/usr/bin/docker inspect -f '{{.State.Status}}' "${service_name}" 2>/dev/null)

    if [ "${res}" = "running" ] 
    then
        result_service_status_code="1"
        result_service_status="started"
    else
        result_service_status_code="0"
        result_service_status="stopped"
    fi
}

function get_service_startup {
    local res
    res=$(/usr/bin/docker inspect -f '{{.HostConfig.RestartPolicy.Name}}' "${service_name}" 2>/dev/null)

    if [ "${res}" = "always" ]; then
        result_service_startup_code="1"
        result_service_startup="enabled"
    else
        result_service_startup_code="0"
        result_service_startup="disabled"
    fi
}

get_service_status
get_service_startup

echo "${service_name}|${service_type}|${result_service_status_code}|${result_service_status}|${result_service_startup_code}|${result_service_startup}"
