#!/bin/bash

function parse_plugin_return {
    IFS="|" read -a myarray <<< "${1}"

    if [ "${myarray[2]}" = "1" ]; then
        col2_color=$conf_color_ok
    elif [ "${myarray[2]}" = "2" ]; then
        col2_color=$conf_color_other
    else
        col2_color=$conf_color_ko
    fi

    if [ "${myarray[4]}" = "1" ]; then
        col3_color=$conf_color_ok
    elif [ "${myarray[4]}" = "2" ]; then
        col3_color=$conf_color_other
    else
        col3_color=$conf_color_ko
    fi

    LINE="${myarray[0]}|${conf_color_name}${myarray[1]}${ENDCOLOR}${conf_col_separator}${col2_color}${myarray[3]}${ENDCOLOR}${conf_col_separator}${col3_color}${myarray[5]}${ENDCOLOR}
"
}
