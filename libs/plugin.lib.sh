#!/bin/bash

source ./libs/config.lib.sh

function init_plugin {
    service_name=$(basename ${1})
}

function format_res {
    printf "%s " "${conf_col_separator}"
    printf "%6s %s" "${1}" "${conf_col_separator}"
    printf "%6s %s" "${2}" "${conf_col_separator}"
    printf "%6s %s" "${3}" "${conf_col_separator}"
    printf "%6s %s" "${4}" "${conf_col_separator}"
    printf "\n"
}

init_plugin "${0}"
